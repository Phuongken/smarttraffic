﻿
    $(document).ready(function () {
        $("#btn-requestEmergency").off('click').on('click', function (e) {
            var Location = $('#Location').val();
            var Name = $('#Name').val();
            var Address = $('#Address').val();
            var EmailRequest = $('#EmailRequest').val();
            var Phone = $('#Phone').val();
            var regex = /^([a-zA-Z0-9_.+-])+\@@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;


            if (Location == '') {
                $("#Location").val("");
                $("#Location").attr("placeholder", "You have not entered a location.");
                $("#Location").addClass('placehoder');
            }

            if (Phone == '') {
                $("#Phone").val("");
                $("#Phone").attr("placeholder", "You have not entered a location.");
                $("#Phone").addClass('placehoder');
            }

            if (Address == '') {
                $("#Address").val("");
                $("#Address").attr("placeholder", "You have not entered a address.");
                $("#Address").addClass('placehoder');
            }

            if (EmailRequest == '') {
                $("#EmailRequest").val("");
                $("#EmailRequest").attr("placeholder", "You have not entered the recipient email.");
                $("#EmailRequest").addClass('placehoder');
            }

            if (Location == '' || Phone == '' || Name == '' || Address == '' || EmailRequest == '') {
                alert("ngu");
            } else {
                e.preventDefault();
                $.ajax({
                    url: '/Home/RequestEmergency',
                    data: {
                        Location: $("#Location").val(),
                        Phone: $("#Phone").val(),
                        Name: $("#Name").val(),
                        Address: $("#Address").val(),
                        EmailRequest: $("#EmailRequest").val()
                    },
                    dataType: 'json',
                    type: 'POST',
                    success: function (res) {
                        if (res.status == true) {
                            swal("Notification", "You have successfully sent contact information. We will contact you soon.", "success")
                                .then((value) => {
                                    window.location.href = "/Home/Index";
                                });
                        } else {
                            swal({
                                title: "Notification",
                                text: "Error. Your contact has not been sent. Please try again!",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true,
                            })
                        }
                    }
                });
            }
        });
    });
