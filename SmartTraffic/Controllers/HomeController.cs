﻿using Model.DAO;
using Model.EF;
using SmartTraffic.Common;
using SmartTraffic.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using static SmartTraffic.Areas.AdminManager.Models.GoogleLatLnqJson;

namespace SmartTraffic.Controllers
{
    public class HomeController : Controller
    {
        UserDao dao = new UserDao();
        DataModel db = new DataModel();
        NewDao new1 = new NewDao();
        FeedbackDao fd = new FeedbackDao();
        public ActionResult Index()
        {
            Session["CheckUserRegister"] = null;
            Session["CheckUserRegister1"] = null;
            Session["CheckUserRegister2"] = null;
            Session["Activate"] = null;
            Session["Activate1"] = null;
            ViewBag.CheckLogin = 3;
            ViewBag.DataNotifyCation = JsonConvert.SerializeObject((from n in db.Notifications where n.DisplayMap == 1 select n).ToList());
            ViewBag.Complain = JsonConvert.SerializeObject((from c in db.Complains where c.DisplayMap == 1 select c).ToList());
            //var model = new1.ListNew();
            var userSession = (UserLogin)Session["UserLogin"];
            ViewBag.Question = fd.ListFeedback();
            return View();
        }


        [HttpGet]
        public JsonResult LoadData(int page, int pageSize = 3)
        {
            IQueryable<New> model = db.News;
            int totalRow = model.Count();
            model = model.OrderByDescending(x => x.CreateDate)
              .Skip((page - 1) * pageSize)
              .Take(pageSize);
            return Json(new
            {
                value = model,
                total = totalRow,
                status = true
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult LoadDataNotification(int page, int pageSize = 3)
        {
            IQueryable<Notification> model = db.Notifications;
            int totalRow = model.Count();
            model = model.OrderByDescending(x => x.CreateDate)
              .Skip((page - 1) * pageSize)
              .Take(pageSize);
            return Json(new
            {
                value = model,
                total = totalRow,
                status = true
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Register(User user)
        {
            if (ModelState.IsValid)
            {
                var user1 = dao.Register(user);
                if (user1 != null)
                {
                    string content = System.IO.File.ReadAllText(Server.MapPath("/Views/Home/VerifyEmail.cshtml"));
                    content = content.Replace("{{FirstName}}", user.FirstName);
                    content = content.Replace("{{LastName}}", user.LastName);
                    content = content.Replace("{{UserID}}", user1.ID.ToString());
                    content = content.Replace("{{Salt}}", user1.Salt.ToString());


                    SendMail mail = new SendMail();
                    var result = mail.SendEmail(user.Email, "Verify Email.", content);
                    if (result == true)
                    {
                        Session["CheckUserRegister"] = 1;
                        return View("Index");
                    }
                }
                Session["CheckUserRegister1"] = 1;
                return View("Index");
            }
            else
            {
                Session["CheckUserRegister2"] = 1;
                return View("Index");
            }
        }

        public ActionResult VerifyEmail()
        {
            return View();
        }

        public ActionResult ActivateEmail(int id, string Salt)
        {
            var result = dao.Active(id, Salt);
            if (result == true)
            {
                Session["Activate"] = 1;
                return View("Index");
            }
            else
            {
                Session["Activate1"] = 1;
                return View("Index");
            }
        }

        public ActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var result = dao.Login(model.Email, model.PassWord);
                if (result == -1)
                {
                    ViewBag.CheckLogin = -1;
                    return View("Index");
                }
                else if (result == 0)
                {
                    ViewBag.CheckLogin = 0;
                    return View("Index");
                }
                else if (result == 2)
                {
                    ViewBag.CheckLogin = 2;
                    return View("Index");
                }
                else if (result == 1)
                {
                    UserLogin user = new UserLogin();
                    var user1 = dao.Find(model.Email);
                    user.Email = user1.Email;
                    user.FirstName = user1.FirstName;
                    user.LastName = user1.LastName;
                    user.UserID = user1.ID;
                    user.AddressDetail = user1.AddressDetail;
                    user.Phone = user1.Phone;


                    Session["UserLogin"] = user;
                    return Redirect("/");
                }
                else
                {
                    ViewBag.CheckLogin = -2;
                    return View("Index");
                }
            }
            else
            {
                ViewBag.CheckLogin = -2;
                return View("Index");
            }
        }


        [HttpPost]
        public JsonResult RequestEmergency(RequestEmergencyViewModel model)
        {
            var webClient = new WebClient();
            var json = webClient.DownloadString(@"https://maps.googleapis.com/maps/api/geocode/json?address=" + model.Location + "&key=AIzaSyAKORLziVDRpaIlRs7NrPwGsye9NNn6Mdw");
            var parsedObj = JsonConvert.DeserializeObject<Rootobject>(json);
            var location = parsedObj.results?[0].geometry?.location;
            var dao = new RequestEmergencyDao();
            var userSession = (UserLogin)Session["UserLogin"];
            RequestEmergency rm = new RequestEmergency()
            {
                Location = model.Location,
                UserID = userSession.UserID,
                Name = model.Name,
                Address = model.Address,
                Email = model.Email,
                Phone = model.Phone,
                lat = location.lat,
                lng = location.lng,
                Status = 1,
                BrowserBy = 1,
                DisplayMap = 1,
                CreateDate = DateTime.Now
            };
            long id = dao.Insert(rm);

            if (id > 0)
            {
                return Json(new
                {
                    status = true
                });
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }
        }

        [HttpPost]
        public JsonResult RequestContact(ContactViewModel model)
        {
            var dao = new ContactDao();
            var userSession = (UserLogin)Session["UserLogin"];
            Feedback ad = new Feedback()
            {
                Description = model.Description,
                UserID = userSession.UserID,
                Status = 1,
                BrowserBy = 1,
                CreateDate = DateTime.Now
            };
            long id = dao.Insert(ad);
            if (id > 0)
            {
                return Json(new
                {
                    status = true
                });
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }
        }

        public JsonResult GetSearchingData(string SearchBy, string SearchValue, int page, int pageSize = 3)
        {
            IQueryable<New> model = db.News;


            if (SearchBy == "Name")
            {
                model = model.Where(x => x.Name.Contains(SearchValue) && x.Status == 1);
            }
            else if (SearchBy == "Content")
            {
                model = model.Where(x => x.Content.Contains(SearchValue) && x.Status == 1);
            }
            else
            {
                model = model.Where(x => x.Title.Contains(SearchValue) && x.Status == 1);
            }

           

            int totalRow = model.Count();

            model = model.OrderByDescending(x => x.CreateDate)
              .Skip((page - 1) * pageSize)
              .Take(pageSize);


            return Json(new
            {
                value = model,
                total = totalRow,
                status = true
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSearchingDataNotification(string SearchBy1, string SearchValue1, int page, int pageSize = 3)
        {
            IQueryable<Notification> model = db.Notifications;


            if (SearchBy1 == "Location")
            {
                model = model.Where(x => x.Location.Contains(SearchValue1) && x.Status == 1);
            }
            
            else
            {
                model = model.Where(x => x.Description.Contains(SearchValue1) && x.Status == 1);
            }

            int totalRow = model.Count();

            model = model.OrderByDescending(x => x.CreateDate)
              .Skip((page - 1) * pageSize)
              .Take(pageSize);


            return Json(new
            {
                value = model,
                total = totalRow,
                status = true
            }, JsonRequestBehavior.AllowGet);
        }
    }
}