﻿var homeconfig = {
    pageSize: 3,
    pageIndex: 1,
}
var homeController = {
    init: function () {
        homeController.loadData();
    },

    loadData: function (changePageSize) {
        $.ajax({
            url: '/Home/LoadData',
            type: 'GET',
            data: {
                page: homeconfig.pageIndex,
                pageSize: homeconfig.pageSize
            },
            dataType: 'json',
            success: function (response) {
                if (response.status) {
                    var data = response.value;
                    var result1 = "";
                    var result2 = "";
                    var template = $('#data-template').html();
                    $.each(data, function (i, value) {
                        result1 += Mustache.render(template, {
                            ID: value.ID,
                            Name: value.Name,
                            Image: value.Image,
                            Title: value.Title
                        });;

                        result2 += '<div class="modal fade" id="exampleModal_' + value.ID + '" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel_' + value.ID+'"'
                            + 'aria-hidden="true" >'
                            + ' <div class="modal-dialog modal-dialog-centered" role="document">'
                            + '  <div class="modal-content">'
                            + ' <div class="modal-header bg-theme">'
                            + ' <h5 class="modal-title" id="exampleModalLabel_' + value.ID + '">' + value.Name+'</h5 >'
                            + ' <button type="button" class="close" data-dismiss="modal" aria-label="Close">'
                            + '<span aria-hidden="true">&times;</span>'
                            + '</button>'
                            + '</div>'
                            + ' <div class="modal-body text-center">'
                            + '<img src="' + value.Image +'" class="img-fluid" alt="" />'
                            + '<p class="text-left my-4">'
                            + value.Content
                            + '</p>'
                            + '<div class="fb-comments" data-href="http://localhost:58424/Home/Index/News/_' + value.ID+ '" data-numposts="5" width="100 % "></div>'
                            + ' </div>'
                            + '</div>'
                            + ' </div>'
                            + ' </div >';
                    });
                    $("#DataSearching").html(result1);
                    $("#DialogNews").html(result2);
                    homeController.paging(response.total, function () {
                        homeController.loadData();
                    }, changePageSize);
                }
            }
        })
    },
    paging: function (totalRow, callback, changePageSize) {
        var totalPage = Math.ceil(totalRow / homeconfig.pageSize);

        //Unbind pagination if it existed or click change pagesize
        if ($('#pagination a').length === 0 || changePageSize === true) {
            $('#pagination').empty();
            $('#pagination').removeData("twbs-pagination");
            $('#pagination').unbind("page");
        }

        $('#pagination').twbsPagination({
            totalPages: totalPage,
            first: "First  |",
            next: " Next  |",
            last: " End  |",
            prev: " Before  |",
            visiblePages: 10,
            onPageClick: function (event, page) {
                homeconfig.pageIndex = page;
                setTimeout(callback, 200);
            }
        });
    }
}
homeController.init();