﻿$('.open_Notificateion').off('click').on('click', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var con = confirm("Do you want to open this notification?");
    if (con) {
        $.ajax({
            url: "/AdminManager/NotificationAdmin/ChangStatusOpen/" + id,
            type: "POST",
            dataType: "json",
            success: function (res) {
                if (res.status == true) {
                    swal("Thông báo", "Open notification successfully.", "success")
                        .then((value) => {
                            window.location.href = "/Notification";
                        });
                } else {
                    swal({
                        title: "Thông báo",
                        text: "Open notification failed.",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true
                    })
                        .then((value) => {
                            window.location.href = "/Notification";
                        });
                }
            }
        });
    }
});