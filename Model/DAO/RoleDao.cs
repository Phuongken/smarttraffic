﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.EF;

namespace Model.DAO
{
    public class RoleDao
    {
        DataModel db = null;
        public RoleDao()
        {
            db = new DataModel();
        }
        public List<Role> ListRole()
        {
            List<Role> result = db.Roles.Where(x => x.Status == 1).ToList();
            return result;
        }
        public bool Insert(Role role)
        {
            try
            {
                db.Roles.Add(role);
                role.Status = 1;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public Role Find(int? id)
        {
            return db.Roles.Find(id);
        }
        public bool Update(Role newRole)
        {
            try
            {
                var role = db.Roles.Find(newRole.ID);
                role.Name = newRole.Name;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool Delete(int? id)
        {
            try
            {
                var role = db.Roles.Find(id);
                role.Status = 0;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
