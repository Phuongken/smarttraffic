namespace Model.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CategoryNew")]
    public partial class CategoryNew
    {
        public int ID { get; set; }

        [StringLength(250)]
        public string Name { get; set; }

        public int? Status { get; set; }
    }
}
