﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.EF;

namespace Model.DAO
{
    public class NotificationDao
    {
        DataModel db = null;
        public NotificationDao()
        {
            db = new DataModel();
        }
        public List<Notification> ListNotification()
        {
            List<Notification> result = db.Notifications.ToList();
            return result;
        }
        public bool Insert(Notification notification, int adminID)
        {
            try
            {
                db.Notifications.Add(notification);
                notification.DisplayMap = 1;
                notification.Status = 1;
                notification.UpdateBy = adminID;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool Update(Notification newNotification, int adminID)
        {
            try
            {
                var notification = db.Notifications.Find(newNotification.ID);
                notification.Date = newNotification.Date;
                notification.Time = newNotification.Time;
                notification.Location = newNotification.Location;
                notification.Description = newNotification.Description;
                notification.UpdateBy = adminID;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool Delete(int? id)
        {
            try
            {
                var role = db.Notifications.Find(id);
                role.Status = 0;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public Notification Find(int id)
        {
            return db.Notifications.Find(id);
        }
        //public string getEmailUser(int id)
        //{
        //    var result = (from u in db.Users where u.ID == id select u.Email).FirstOrDefault();
        //    return result;
        //}
        public string getEmailUserCreate()
        {
            var result = (from u in db.Users select u.Email).FirstOrDefault();
            return result;
        }
        public bool ChangeStatusSend(int id, int adminID)
        {
            try
            {
                var notification = db.Notifications.Find(id);
                if (notification.Status == 0)
                {
                    notification.UpdateBy = adminID;
                    notification.Status = 1;
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool ChangeStatusOpen(int id, int adminID)
        {
            try
            {
                var notification = db.Notifications.Find(id);
                if (notification.Status == 0)
                {
                    notification.UpdateBy = adminID;
                    notification.Status = 1;
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
