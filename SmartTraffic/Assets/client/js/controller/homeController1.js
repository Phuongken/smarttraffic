﻿var homeconfig1 = {
    pageSize: 3,
    pageIndex: 1,
}
var homeController1 = {
    init: function () {
        homeController1.loadData1();
    },
    loadData1: function (changePageSize) {
        $.ajax({
            url: '/Home/LoadDataNotification',
            type: 'GET',
            data: {
                page: homeconfig1.pageIndex,
                pageSize: homeconfig1.pageSize
            },
            dataType: 'json',
            success: function (response) {
                if (response.status) {
                    var data = response.value;
                    var result1 = "";
                    var result2 = "";
                    var template = $('#data-template1').html();
                    $.each(data, function (i, value) {
                        function parseDate1(date) {
                            var str = date;
                            var res1 = str.toString().replace("/Date(", '');
                            var res2 = res1.toString().replace(")/", '');
                            var newDate = new Date(parseInt(res2));
                            newDate.setDate(newDate.getDate());
                            return newDate;
                        };

                        function datetimeNew(date) {
                            var dd = parseDate1(date).getDate();
                            var mm = parseDate1(date).getMonth() + 1;
                            var y = parseDate1(date).getFullYear();
                            return end = dd + '-' + mm + '-' + y;
                        };

                        result1 += Mustache.render(template, {
                            ID: value.ID,
                            Description: value.Description,
                            Location: value.Location,
                            Date: datetimeNew(value.Date)
                        });
                        


                        result2 += '<div class="modal fade" id="exampleModal_n' + value.ID + '" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel_n' + value.ID + '"'
                            + 'aria-hidden="true" >'
                            + ' <div class="modal-dialog modal-dialog-centered" role="document">'
                            + '  <div class="modal-content">'
                            + ' <div class="modal-header bg-theme">'
                            + ' <h5 class="modal-title" id="exampleModalLabel_n' + value.ID + '">' +"Location: " + value.Location + '</h5 >'
                            + ' <button type="button" class="close" data-dismiss="modal" aria-label="Close">'
                            + '<span aria-hidden="true">&times;</span>'
                            + '</button>'
                            + '</div>'
                            + ' <div class="modal-body text-center">'
                            + '<p class="text-left my-4">'
                            + value.Description
                            + '</p>'
                            + '<p> Create Date: ' + datetimeNew(value.Date) +'</p>'
                            + '<div class="fb-comments" data-href="http://localhost:58424/Home/Index/Notifiaction/_' + value.ID + '" data-numposts="5" width="100 % "></div>'
                            + ' </div>'
                            + '</div>'
                            + ' </div>'
                            + ' </div >';
                    });
                    $("#DataSearching1").html(result1);
                    $("#DialogNotifications").html(result2);
                    homeController1.paging1(response.total, function () {
                        homeController1.loadData1();
                    }, changePageSize);
                }
            }
        })
    },
    paging1: function (totalRow, callback, changePageSize) {
        var totalPage = Math.ceil(totalRow / homeconfig1.pageSize);

        //Unbind pagination if it existed or click change pagesize
        if ($('#pagination1 a').length === 0 || changePageSize === true) {
            $('#pagination1').empty();
            $('#pagination1').removeData("twbs-pagination");
            $('#pagination1').unbind("page");
        }

        $('#pagination1').twbsPagination({
            totalPages: totalPage,
            first: "First  |",
            next: " Next  |",
            last: " End  |",
            prev: " Before  |",
            visiblePages: 10,
            onPageClick: function (event, page) {
                homeconfig1.pageIndex = page;
                setTimeout(callback, 200);
            }
        });
    }
}
homeController1.init();