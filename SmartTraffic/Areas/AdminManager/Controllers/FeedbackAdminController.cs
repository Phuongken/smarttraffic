﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model.DAO;
using Model.EF;
using SmartTraffic.Common;
using SmartTraffic.Models;

namespace SmartTraffic.Areas.AdminManager.Controllers
{
    public class FeedbackAdminController : Controller
    {
        DataModel db = new DataModel();
        FeedbackDao dao = new FeedbackDao();
        // GET: AdminManager/FeedbackAdmin
        public ActionResult Index()
        {
            var result = dao.ListFeedback();
            ViewBag.getUserOfFeedback = dao.getFeedbackOfUser();
            return View(result);
        }
        public ActionResult emailFeedback()
        {
            return View();
        }
        public JsonResult SendMailFeedback(int id)
        {
            SendMail sendMail = new SendMail();
            var result1 = dao.getEmailFeedback(id);
            var txtNew = dao.Find(id);
            var admin = (AdminLogin)Session["AdminSession"];
            var adminID = admin.UserID;
            var getUser = (from c in db.Feedbacks join o in db.Users on c.UserID equals o.ID where c.ID == id select o).FirstOrDefault();
            var result = dao.ChangeStatusSend(id, Convert.ToInt32(adminID));
            if (result == true)
            {
                string content = System.IO.File.ReadAllText(Server.MapPath("/Areas/AdminManager/Views/FeedbackAdmin/emailFeedback.cshtml"));
                content = content.Replace("{{ID}}", txtNew.ID.ToString());
                content = content.Replace("{{FirstName}}", getUser.FirstName);
                content = content.Replace("{{LastName}}", getUser.LastName);
                content = content.Replace("{{Email}}", getUser.Email);
                sendMail.SendEmail(result1, "Smart traffic team notificate", content);

                Session["SendMailSuccsess"] = 1;
                return Json(new
                {
                    status = true
                });
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }
        }
    }
}