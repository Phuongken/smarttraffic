﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model.DAO;
using SmartTraffic.Areas.AdminManager.Models;
using SmartTraffic.Common;

namespace SmartTraffic.Areas.AdminManager.Controllers
{
    public class LoginAdminController : Controller
    {
        AdminDao dao = new AdminDao();
        // GET: AdminManager/LoginAdmin
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Login(LoginModelAdmin admin)
        {
            if (ModelState.IsValid)
            {
                var result = dao.Login(admin.Email, admin.Password);
                if (result == true)
                {
                    var loginAdmin = dao.GetById(admin.Email);
                    var adminSession = new AdminLogin();
                    adminSession.Email = loginAdmin.Email;
                    adminSession.UserID = loginAdmin.ID;
                    Session["AdminSession"] = adminSession;
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Tài khoản hoặc mật khẩu chưa chính xác");
                }
            }
            return View("Index");
        }
    }
}