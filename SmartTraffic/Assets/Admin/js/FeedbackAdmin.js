﻿$('.btn-feedback').off('click').on('click', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var con = confirm("Do you want to send feedback to users?");
    if (con) {
        $.ajax({
            url: "/AdminManager/FeedbackAdmin/SendMailFeedback/" + id,
            type: "POST",
            dataType: "json",
            success: function (res) {
                if (res.status == true) {
                    swal("Thông báo", "Send feedback to users successfully.", "success")
                        .then((value) => {
                            window.location.href = "/Feedback";
                        });
                } else {
                    swal({
                        title: "Thông báo",
                        text: "Send feedback failed.",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true
                    })
                        .then((value) => {
                            window.location.href = "/Feedback";
                        });
                }
            }
        });
    }
});