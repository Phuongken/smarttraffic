﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartTraffic.Models
{
    public class ContactViewModel
    {
        public int UserID { get; set; }

        public string Description { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}