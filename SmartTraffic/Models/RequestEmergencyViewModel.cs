﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartTraffic.Models
{
    public class RequestEmergencyViewModel
    {
        public string Location { get; set; }

        public long UserID { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string Email { get; set; }

        public int Phone { get; set; }

        public double lat { get; set; }

        public double lng { get; set; }

        public DateTime CreateDate { get; set; }
    }
}