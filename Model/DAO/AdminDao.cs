﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.EF;

namespace Model.DAO
{

    public class AdminDao
    {
        DataModel db = null;
        public AdminDao()
        {
            db = new DataModel();
        }
        public bool Register(Admin admin)
        {
            try
            {
                if (db.Admins.Any(x => x.Email == admin.Email))
                {
                    return false;
                }
                string salt = Guid.NewGuid().ToString().Substring(0, 7);
                string pass = admin.Password;
                string MD5Hash = Encryptor.MD5Hash(pass + salt);
                admin.Salt = salt;
                admin.Password = MD5Hash;
                admin.CreateDate = DateTime.Now;
                admin.UpdateDate = DateTime.Now;
                admin.Status = 1;
                db.SaveChanges();
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool Login(string email, string password)
        {
            var result = db.Admins.SingleOrDefault(x => x.Email == email);
            if (result == null)
            {
                return false;
            }
            else
            {
                if (result.Status == 0)
                {
                    return false;
                }
                else
                {
                    string salt = result.Salt.ToString();
                    var MD5Hash = Encryptor.MD5Hash(password + salt);
                    if (result.Password == MD5Hash)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }
        public Admin GetById(string email)
        {
            return db.Admins.SingleOrDefault(x => x.Email == email);
        }
        public Admin Find(int? id)
        {
            return db.Admins.Find(id);
        }
    }
}
