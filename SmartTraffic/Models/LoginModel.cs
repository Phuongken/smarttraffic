﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartTraffic.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Mời bạn nhập User Name")]
        public string Email { set; get; }

        [Required(ErrorMessage = "Mời bạn nhập Password")]
        public string PassWord { set; get; }

    }
}