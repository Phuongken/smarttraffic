namespace Model.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Complain")]
    public partial class Complain
    {
        public int ID { get; set; }

        public int UserID { get; set; }

        [StringLength(50)]
        public string Title { get; set; }

        public int? CategoryComplainID { get; set; }

        [Column(TypeName = "text")]
        public string Description { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? UpdateDate { get; set; }

        public int? BrowserBy { get; set; }

        public int? Status { get; set; }
        public Double lat { get; set; }
        public Double lng { get; set; }
        public int DisplayMap { get; set; }
    }
}
