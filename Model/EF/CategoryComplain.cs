namespace Model.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CategoryComplain")]
    public partial class CategoryComplain
    {
        public int ID { get; set; }

        [StringLength(150)]
        public string Name { get; set; }

        public int? Status { get; set; }
    }
}
