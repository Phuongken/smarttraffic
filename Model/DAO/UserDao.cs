﻿using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.DAO
{
    public class UserDao
    {
        DataModel db = null;

        public UserDao()
        {
            db = new DataModel();
        }

        public User Register(User user)
        {
            try
            {
                Random rand = new Random((int)DateTime.Now.Ticks);
                int numIterations = 0;
                numIterations = rand.Next(1, 1000000000);
                if (db.Users.Any(x=>x.ID == numIterations))
                {
                    return null;
                }
                if(db.Users.Any(x=>x.Email == user.Email))
                {
                    return null;
                }
                string salt = Guid.NewGuid().ToString().Substring(0, 7);
                string pass = user.Password;

                string MD5Hash = Encryptor.MD5Hash(pass + salt);

                user.ID = numIterations;
                user.Password = MD5Hash;
                user.Salt = salt;
                user.Status = -1;
                user.CreateDate = DateTime.Now;
                user.UpdateDate = DateTime.Now;
                user.UpdateBy = 0;

                db.Users.Add(user);
                db.SaveChanges();
                return user;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool Active(int id, string Salt)
        {
            try
            {
                var result = db.Users.Where(x => x.ID == id && x.Salt == Salt && x.Status == -1).FirstOrDefault();
                result.Status = 2;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public int Login(string Email, string Password)
        {
            try
            {
                var result = db.Users.SingleOrDefault(x => x.Email == Email);
                if (result == null)
                {
                    return -2;
                } 
                else
                {
                    if (result.Status == -1)
                    {
                        return -1;
                    }
                    else if (result.Status == 2)
                    {
                        return 2;
                    }
                    else if (result.Status == 0)
                    {
                        return 0;
                    }
                    else
                    {
                        string salt = result.Salt.ToString();
                        var MD5Hash = Encryptor.MD5Hash(Password + salt);
                        if (result.Password == MD5Hash)
                            return 1;
                        else
                            return -2;
                    }
                }

            }
            catch
            {
                return -2;
            }
        }

        public User Find(string email)
        {
            return db.Users.Where(x => x.Email == email).FirstOrDefault();
        }
    }
}
