﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.EF;

namespace Model.DAO
{
    public class ManagerUserDao
    {
        DataModel db = null;
        public ManagerUserDao()
        {
            db = new DataModel();
        }
        //Phê duyệt user
        public bool ChangeStatusApproved(int id, int adminID)
        {
            try
            {

                var user = db.Users.Find(id);
                if (user.Status == 2)
                {
                    Random rand = new Random((int)DateTime.Now.Ticks);
                    long numIterations = 0;
                    numIterations = rand.Next(1, 1000000000);
                    if (db.Users.Any(x => x.ID == numIterations))
                    {
                        return false;
                    }
                    //int range = 100;
                    //int rDouble = r.Ne() * range; //for doubles
                    //var now = new DateTime();
                    //var randomNum = "";
                    //randomNum += Math.Round(now * 9);
                    //randomNum += Math.Round(Math.Random() * 9);
                    //randomNum += now.getTime().toString().slice(-16);
                    user.ID = numIterations;
                    user.UpdateBy = adminID;
                    user.Status = 1;
                    //db.Users.Add(user);
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //Từ chối user
        public bool ChangeStatusRefuse(int id, int adminID)
        {
            try
            {
                var user = db.Users.Find(id);
                if (user.Status == 1)
                {
                    user.UpdateBy = adminID;
                    user.Status = 0;
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //Khóa user
        public bool ChangeStatusLock(int id, int adminID)
        {
            try
            {
                var user = db.Users.Find(id);
                if (user.Status == 1)
                {
                    user.UpdateBy = adminID;
                    user.Status = 0;
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //Mở lại user
        public bool ChangeStatusReopen(int id, int adminID)
        {
            try
            {
                var user = db.Users.Find(id);
                if (user.Status == 0)
                {
                    user.UpdateBy = adminID;
                    user.Status = 1;
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public string getUserByMail(int id)
        {
            var result = (from u in db.Users where u.ID == id select u.Email).FirstOrDefault();
            return result;
        }
        public User Find(int id)
        {
            return db.Users.Find(id);
        }
    }
}
