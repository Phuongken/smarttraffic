namespace Model.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Notification")]
    public partial class Notification
    {
        public int ID { get; set; }

        public DateTime? Date { get; set; }

        public TimeSpan? Time { get; set; }

        [StringLength(250)]
        public string Location { get; set; }

        [Column(TypeName = "text")]
        public string Description { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? UpdateDate { get; set; }

        public int? CreateBy { get; set; }

        public int? UpdateBy { get; set; }

        public int Status { get; set; }
        public Double lat { get; set; }
        public Double lng { get; set; }
        public int DisplayMap { get; set; }
    }
}
