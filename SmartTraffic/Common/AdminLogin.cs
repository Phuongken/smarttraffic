﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartTraffic.Common
{
    [Serializable]
    public class AdminLogin
    {
        public long UserID { get; set; }
        public string Email { get; set; }
    }
}