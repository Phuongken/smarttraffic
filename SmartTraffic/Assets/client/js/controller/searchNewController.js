﻿$(document).ready(function () {
    var homeconfig = {
        pageSize: 3,
        pageIndex: 1,
    };
    $("#SearchBtn").off('click').on('click', function () {
        var SearchBy = $("#SearchBy").val();
        var SearchValue = $("#SearchValue").val();
        $.ajax({
            type: "post",
            url: "/Home/GetSearchingData?SearchBy=" + SearchBy + "&SearchValue=" + SearchValue + "&page=" + homeconfig.pageIndex + "&pageSize=" + homeconfig.pageSize,
            contentType: "html",
            success: function (result) {
                if (result.value.length == 0) {

                    result1 = '<p style="text-align:center; color: red;">No Data</p>';
                    $("#DataSearching").html(result1);
                } else {
                    var result1 = "";
                    var result2 = "";
                    var template = $('#data-template').html();
                    $.each(result.value, function (i, value) {
                        result1 += Mustache.render(template, {
                            ID: value.ID,
                            Name: value.Name,
                            Image: value.Image,
                            Title: value.Title
                        });

                        result2 += '<div class="modal fade" id="exampleModal_' + value.ID + '" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel_' + value.ID + '"'
                            + 'aria-hidden="true" >'
                            + ' <div class="modal-dialog modal-dialog-centered" role="document">'
                            + '  <div class="modal-content">'
                            + ' <div class="modal-header bg-theme">'
                            + ' <h5 class="modal-title" id="exampleModalLabel_' + value.ID + '">' + value.Name + '</h5 >'
                            + ' <button type="button" class="close" data-dismiss="modal" aria-label="Close">'
                            + '<span aria-hidden="true">&times;</span>'
                            + '</button>'
                            + '</div>'
                            + ' <div class="modal-body text-center">'
                            + '<img src="' + value.Image + '" class="img-fluid" alt="" />'
                            + '<p class="text-left my-4">'
                            + value.Content
                            + '</p>'
                            + '<div class="fb-comments" data-href="http://localhost:58424/Home/Index/News/_' + value.ID + '" data-numposts="5" width="100 % "></div>'
                            + ' </div>'
                            + '</div>'
                            + ' </div>'
                            + ' </div >';
                    });
                    $("#DataSearching").html(result1);
                    $("#DialogNews").html(result2);
                }
            },
        });
    });
});