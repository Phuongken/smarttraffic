﻿$(document).ready(function () {
    $("#btn-contact").off('click').on('click', function (e) {
        e.preventDefault();
        var DescriptionContact = $('#DescriptionContact').val();
        var regex = /^([a-zA-Z0-9_.+-])+\@@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;


        if (DescriptionContact.length < 5) {
            $("#DescriptionCongtact").val("");
            $("#DescriptionContact").attr("placeholder", "You have not entered the content");
            $("#DescriptionContact").addClass('placehoder');
        } else if (DescriptionContact == '') {
            $("#DescriptionContact").val("");
            $("#DescriptionContact").attr("placeholder", "You have not entered the content");
            $("#DescriptionContact").addClass('placehoder');
        }

        if (DescriptionContact == '' || DescriptionContact.length < 5
        ) {

        } else {
            
            $.ajax({
                url: '/Home/RequestContact',
                data: {
                    Description: $("#DescriptionContact").val(),
                },
                dataType: 'json',
                type: 'POST',
                success: function (res) {
                    if (res.status == true) {
                        swal("Notification", "You have successfully sent contact information. We will contact you soon.", "success")
                            .then((value) => {
                                window.location.href = "/Home/Index";
                            });
                    } else {
                        swal({
                            title: "Notification",
                            text: "Error. Your contact has not been sent. Please try again!",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                        })
                    }
                }
            });
        }
    });
});