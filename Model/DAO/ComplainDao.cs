﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.EF;

namespace Model.DAO
{
    public class ComplainDao
    {
        DataModel db = null;
        public ComplainDao()
        {
            db = new DataModel();
        }
        public List<Complain> ListComplain()
        {
            List<Complain> result = db.Complains.ToList();
            return result;
        }
        public bool ChangeStatusDone(int id, int adminID)
        {
            try
            {
                var complain = db.Complains.Find(id);
                if (complain.Status == 1)
                {
                    complain.BrowserBy = adminID;
                    complain.Status = 2;
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //
        public bool ChangeStatusProcessing(int id, int adminID)
        {
            try
            {
                var complain = db.Complains.Find(id);
                if (complain.Status == 0)
                {
                    complain.BrowserBy = adminID;
                    complain.Status = 1;
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public string getComplain(int id)
        {
            var result = (from c in db.Complains join u in db.Users on c.UserID equals u.ID where c.ID == id select u.Email).FirstOrDefault();
            return result;
        }
        public Complain Find(int id)
        {
            return db.Complains.Find(id);
        }
        public List<User> getUserOfComplain()
        {
            List<User> result = (from c in db.Users select c).ToList();
            return result;
        }
        
    }
}
