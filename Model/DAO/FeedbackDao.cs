﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.EF;

namespace Model.DAO
{
    public class FeedbackDao
    {
        DataModel db = null;
        public FeedbackDao()
        {
            db = new DataModel();
        }
        public List<Feedback> ListFeedback()
        {
            var result = db.Feedbacks.ToList();
            return result;
        }
        public List<User> getFeedbackOfUser()
        {
            List<User> result = (from u in db.Users select u).ToList();
            return result;
        }
        public string getEmailFeedback(int id)
        {
            var result = (from f in db.Feedbacks join u in db.Users on f.UserID equals u.ID where f.ID == id select u.Email).FirstOrDefault();
            return result;
        }
        public Feedback Find(int id)
        {
            return db.Feedbacks.Find(id);
        }
        public bool ChangeStatusSend(int id, int adminID)
        {
            try
            {
                var feedback = db.Feedbacks.Find(id);
                if (feedback.Status == 0)
                {
                    feedback.BrowserBy= adminID;
                    feedback.Status = 1;
                    feedback.UpdateDate = DateTime.Now;
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
