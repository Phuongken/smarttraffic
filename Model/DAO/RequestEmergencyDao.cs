﻿using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.DAO
{
    public class RequestEmergencyDao
    {
        DataModel db = null;
        public RequestEmergencyDao()
        {
            db = new DataModel();
        }

        public long Insert(RequestEmergency entity)
        {
            entity.UpdateDate = DateTime.Now;
            db.RequestEmergencies.Add(entity);
            db.SaveChanges();
            return entity.ID;
        }
    }
}
