﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Model.DAO;
using Model.EF;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SmartTraffic.Common;
using SmartTraffic.Models;
using static SmartTraffic.Areas.AdminManager.Models.GoogleLatLnqJson;

namespace SmartTraffic.Areas.AdminManager.Controllers
{
    public class NotificationAdminController : Controller
    {
        NotificationDao dao = new NotificationDao();
        // GET: AdminManager/NotificationAdmin
        public ActionResult Index()
        {
            var result = dao.ListNotification();
            return View(result);
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Notification newNotification)
        {
            SendMail sendMail = new SendMail();
            var result1 = dao.getEmailUserCreate();
            //var txtNew = dao.Find();
            var admin = (AdminLogin)Session["AdminSession"];
            var adminID = admin.UserID;
            var webClient = new WebClient();
            var json = webClient.DownloadString(@"https://maps.googleapis.com/maps/api/geocode/json?address=" + newNotification.Location + "&key=AIzaSyAKORLziVDRpaIlRs7NrPwGsye9NNn6Mdw");
            var parsedObj = JsonConvert.DeserializeObject<Rootobject>(json);
            var location = parsedObj.results?[0].geometry?.location;
            newNotification.lat = location.lat;
            newNotification.lng = location.lng;
            newNotification.CreateDate = DateTime.Now;
            newNotification.UpdateDate = DateTime.Now;
            newNotification.CreateBy = 1;
            bool role = dao.Insert(newNotification, Convert.ToInt32(adminID));
            if (role == true)
            {
                string content = System.IO.File.ReadAllText(Server.MapPath("/Areas/AdminManager/Views/NotificationAdmin/emailNotification.cshtml"));
                content = content.Replace("{{ID}}", newNotification.ID.ToString());
                content = content.Replace("{{Date}}", newNotification.Date.ToString());
                content = content.Replace("{{Time}}", newNotification.Time.ToString());
                content = content.Replace("{{Location}}", newNotification.Location);
                content = content.Replace("{{Description}}", newNotification.Description);
                sendMail.SendEmail(result1, "Smart traffic team notificate", content);
                return RedirectToAction("Index", "NotificationAdmin");
            }
            else
            {
                ModelState.AddModelError("", "Create notification failed.");
            }
            return View(newNotification);
        }
        public ActionResult Edit(int id)
        {
            var result = dao.Find(id);
            return View(result);
        }

        // POST: AdminManager/NewAdmin/Edit/5
        [HttpPost]
        public ActionResult Edit(Notification notification, int id)
        {
            SendMail sendMail = new SendMail();
            var result1 = dao.getEmailUserCreate();
            var txtNew = dao.Find(id);
            var admin = (AdminLogin)Session["AdminSession"];
            var adminID = admin.UserID;
            var result = dao.Update(notification, Convert.ToInt32(adminID));
            if (result == true)
            {
                string content = System.IO.File.ReadAllText(Server.MapPath("/Areas/AdminManager/Views/NotificationAdmin/emailNotificationEdit.cshtml"));
                content = content.Replace("{{ID}}", txtNew.ID.ToString());
                content = content.Replace("{{Date}}", txtNew.Date.ToString());
                content = content.Replace("{{Time}}", txtNew.Time.ToString());
                content = content.Replace("{{Location}}", txtNew.Location);
                content = content.Replace("{{Description}}", txtNew.Description);
                sendMail.SendEmail(result1, "Smart traffic team notificate", content);
                return RedirectToAction("Index", "NotificationAdmin");
            }
            else
            {
                ModelState.AddModelError("", "Edit notification failed");
            }
            return View(notification);
        }
        [HttpPost]
        public ActionResult Delete(int? id)
        {
            var result = dao.Delete(id);
            if (result == true)
            {
                return Json(new
                {
                    status = true
                });
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }
        }
        //email notification create
        public ActionResult emailNotification()
        {
            return View();
        }
        //email notification edit
        public ActionResult emailNotificationEdit()
        {
            return View();
        }
        public JsonResult ChangStatusOpen(int id)
        {
            SendMail sendMail = new SendMail();
            var txtNew = dao.Find(id);
            var admin = (AdminLogin)Session["AdminSession"];
            var adminID = admin.UserID;
            var result = dao.ChangeStatusOpen(id, Convert.ToInt32(adminID));
            if (result == true)
            {
                return Json(new
                {
                    status = true
                });
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }

        }
    }
}