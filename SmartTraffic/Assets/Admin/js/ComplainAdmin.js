﻿
$('.btn-Done').off('click').on('click', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var con = confirm("Do you want to resolve this complaint?");
    if (con) {
        $.ajax({
            url: "/AdminManager/ComplainAdmin/ChangeStatus/" + id,
            type: "POST",
            dataType: "json",
            success: function (res) {
                if (res.status == true) {
                    swal("Thông báo", "Complete the complaint", "success")
                        .then((value) => {
                            window.location.href = "/AdminManager/ComplainAdmin/Index";
                        });
                } else {
                    swal({
                        title: "Thông báo",
                        text: "This complaint has not been resolved",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true
                    })
                        .then((value) => {
                            window.location.href = "/AdminManager/ComplainAdmin/Index";
                        });
                }
            }
        });
    }
});
//
$('.btn-Processing').off('click').on('click', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var con = confirm("Do you want to resolve this complaint?");
    if (con) {
        $.ajax({
            url: "/AdminManager/ComplainAdmin/ChangeStatusProcessing/" + id,
            type: "POST",
            dataType: "json",
            success: function (res) {
                if (res.status == true) {
                    swal("Thông báo", "Solving", "success")
                        .then((value) => {
                            window.location.href = "/AdminManager/ComplainAdmin/Index";
                        });
                } else {
                    swal({
                        title: "Thông báo",
                        text: "Not resolved yet",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true
                    })
                        .then((value) => {
                            window.location.href = "/AdminManager/ComplainAdmin/Index";
                        });
                }
            }
        });
    }
});
// chọn theo ngày
$(function () {
    $('input[name="daterange"]').daterangepicker({
        opens: 'left'
    }, function (start, end, label) {
        var search = $("#InputDate").val();
        var id = $("#InsuranceIDFinder").val();
        var idStatus = $("#InsuranceStatusFinder").val();
        var idDuration = $("#InsuranceDurationFinder").val();
        Finter(id, start.format('YYYY-MM-DD'), end.format('YYYY-MM-DD'), search, idStatus, idDuration);
    });
});
$(".Finter").off('click').on('click', function (e) {
    e.preventDefault();
    var id = $(this).data("id");
    var search = $("#InputDate").val();
    var date = $("#DateFinter").val();
    var startDate = date.slice(0, 10);
    var endDate = date.slice(13, 23);
    var idStatus = $("#InsuranceStatusFinder").val();
    var idDuration = $("#InsuranceDurationFinder").val();
    $("#InsuranceIDFinder").val(id);
    Finter(id, startDate, endDate, search, idStatus, idDuration);
    $(".Finter").css("background", "#1b93e1");
    if ($("#InsuranceIDFinder").val() == id) {
        $(this).css("background", "#e74c3c");
    }
});
$("#SearchFinder").off('click').on('click', function (e) {
    e.preventDefault();
    var search = $("#InputDate").val();
    var date = $("#DateFinter").val();
    var startDate = date.slice(0, 10);
    var endDate = date.slice(13, 23);
    var id = $("#InsuranceIDFinder").val();
    var idStatus = $("#InsuranceStatusFinder").val();
    var idDuration = $("#InsuranceDurationFinder").val();
    Finter(id, startDate, endDate, search, idStatus, idDuration);
});

$(".StatusInsurance").off('click').on('click', function (e) {
    e.preventDefault();
    var id = $("#InsuranceIDFinder").val();
    var idStatus = $(this).data("id");
    var idDuration = $("#InsuranceDurationFinder").val();
    var search = $("#InputDate").val();
    var date = $("#DateFinter").val();
    var startDate = date.slice(0, 10);
    var endDate = date.slice(13, 23);
    Finter(id, startDate, endDate, search, idStatus, idDuration);
    $("#InsuranceStatusFinder").val(idStatus);
    $(".StatusInsurance").css("background", "#ff9501");
    if ($("#InsuranceStatusFinder").val() == idStatus) {
        $(this).css("background", "#e74c3c");
    }
});

$('.ExpireInsurance').off('click').on('click', function (e) {
    e.preventDefault();
    var id = $("#InsuranceIDFinder").val();
    var idStatus = $("#InsuranceStatusFinder").val();
    var idDuration = $(this).data("id");
    var search = $("#InputDate").val();
    var date = $("#DateFinter").val();
    var startDate = date.slice(0, 10);
    var endDate = date.slice(13, 23);
    Finter(id, startDate, endDate, search, idStatus, idDuration);
    $("#InsuranceDurationFinder").val(idDuration);
    $(".ExpireInsurance").css("background", "#a2d200");
    if ($("#InsuranceDurationFinder").val() == idDuration) {
        $(this).css("background", "#e74c3c");
    }
});
