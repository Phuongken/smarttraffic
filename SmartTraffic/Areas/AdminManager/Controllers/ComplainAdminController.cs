﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Model.DAO;
using Model.EF;
using SmartTraffic.Areas.AdminManager.Models;
using SmartTraffic.Common;
using SmartTraffic.Models;

namespace SmartTraffic.Areas.AdminManager.Controllers
{
    public class ComplainAdminController : Controller
    {
        ComplainDao dao = new ComplainDao();
        // GET: AdminManager/ComplainAdmin
        public ActionResult Index()
        {
            var complain = dao.ListComplain();
            ViewBag.getInforUser = dao.getUserOfComplain();
            return View(complain);
        }
        public ActionResult StatusDone()
        {
            return View();
        }
        public ActionResult StatusProcessing()
        {
            return View();
        }
        public JsonResult ChangeStatusProcessing(int id)
        {
            SendMail sendMail = new SendMail();
            var result1 = dao.getComplain(id);
            var txtNew = dao.Find(id);
            var admin = (AdminLogin)Session["AdminSession"];
            var adminID = admin.UserID;
            //var getOrderDetail = (from c in db.Contracts join o in db.OrderDetails on c.ID equals o.ContractID where c.ID == id select o).FirstOrDefault();
            var result = dao.ChangeStatusProcessing(id, Convert.ToInt32(adminID));
            if (result == true)
            {

                string content = System.IO.File.ReadAllText(Server.MapPath("/Areas/AdminManager/Views/ComplainAdmin/StatusProcessing.cshtml"));
                content = content.Replace("{{ID}}", txtNew.ID.ToString());
                content = content.Replace("{{Title}}", txtNew.Title);
                content = content.Replace("{{Description}}", txtNew.Description);
                //content = content.Replace("{{Status}}", txtNew.Status.ToString());
                if (txtNew.Status == 2)
                {
                    content = content.Replace("{{Status}}", "Đã giải quyết");
                }
                else if (txtNew.Status == 1)
                {
                    content = content.Replace("{{Status}}", "Đang giải quyết");
                }
                sendMail.SendEmail(result1, "SmartTrafic team", content);

                Session["SendMailSuccsess"] = 1;
                return Json(new
                {
                    status = true
                });
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }

        }
        //
        public JsonResult ChangeStatus(int id)
        {
            SendMail sendMail = new SendMail();
            var result1 = dao.getComplain(id);
            var txtNew = dao.Find(id);
            var admin = (AdminLogin)Session["AdminSession"];
            var adminID = admin.UserID;
            //var getOrderDetail = (from c in db.Contracts join o in db.OrderDetails on c.ID equals o.ContractID where c.ID == id select o).FirstOrDefault();
            var result = dao.ChangeStatusDone(id, Convert.ToInt32(adminID));
            if (result == true)
            {

                string content = System.IO.File.ReadAllText(Server.MapPath("/Areas/AdminManager/Views/ComplainAdmin/StatusDone.cshtml"));
                content = content.Replace("{{ID}}", txtNew.ID.ToString());
                content = content.Replace("{{Title}}", txtNew.Title);
                content = content.Replace("{{Description}}", txtNew.Description);
                if (txtNew.Status == 2)
                {
                    content = content.Replace("{{Status}}", "Đã giải quyết");
                }
                else if (txtNew.Status == 1)
                {
                    content = content.Replace("{{Status}}", "Đang giải quyết");
                }

                sendMail.SendEmail(result1, "SmartTrafic team", content);

                //Session["SendMailSuccsess"] = 1;
                return Json(new
                {
                    status = true
                });
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }

        }
        public ActionResult Details(int id)
        {
            DataModel db = new DataModel();
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            User user = db.Users.Find(id);
            if (user == null)
                return HttpNotFound();
            return View(user);
        }
    }
}