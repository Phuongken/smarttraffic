﻿////auto generate ID user 
//var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
//var stringChars = new char[8];
//var random = new Random();

//for (int i = 0; i < stringChars.Length; i++)
//{
//    stringChars[i] = chars[random.Next(chars.Length)];
//}

//var finalString = new String(stringChars);

// Phê duyệt

$('.btn-pheduyet').off('click').on('click', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var con = confirm("Do you want to accept this account?");
    if (con) {
        $.ajax({
            url: "/AdminManager/ManagerUserAdmin/ChangeStatusApproved/" + id,
            type: "POST",
            dataType: "json",
            success: function (res) {
                if (res.status == true) {
                    swal("Thông báo", "Successful approval", "success")
                        .then((value) => {
                            window.location.href = "/AdminManager/ManagerUserAdmin/Index";
                        });
                } else {
                    swal({
                        title: "Thông báo",
                        text: "Failure to approve",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true
                    })
                        .then((value) => {
                            window.location.href = "/AdminManager/ManagerUserAdmin/Index";
                        });
                }
            }
        });
    }
});
// Từ chối
$('.btn-tuchoi').off('click').on('click', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var con = confirm("Do you want to accept this account?");
    if (con) {
        $.ajax({
            url: "/AdminManager/ManagerUserAdmin/ChangeStatusRefuse/" + id,
            type: "POST",
            dataType: "json",
            success: function (res) {
                if (res.status == true) {
                    swal("Thông báo", "Successful approval", "success")
                        .then((value) => {
                            window.location.href = "/AdminManager/ManagerUserAdmin/Index";
                        });
                } else {
                    swal({
                        title: "Thông báo",
                        text: "Failure to approve",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true
                    })
                        .then((value) => {
                            window.location.href = "/AdminManager/ManagerUserAdmin/Index";
                        });
                }
            }
        });
    }
});
// Khóa
$('.btn-khoa').off('click').on('click', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var con = confirm("Do you want to accept this account?");
    if (con) {
        $.ajax({
            url: "/AdminManager/ManagerUserAdmin/ChangeStatusLock/" + id,
            type: "POST",
            dataType: "json",
            success: function (res) {
                if (res.status == true) {
                    swal("Thông báo", "Successful approval", "success")
                        .then((value) => {
                            window.location.href = "/AdminManager/ManagerUserAdmin/Index";
                        });
                } else {
                    swal({
                        title: "Thông báo",
                        text: "Failure to approve",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true
                    })
                        .then((value) => {
                            window.location.href = "/AdminManager/ManagerUserAdmin/Index";
                        });
                }
            }
        });
    }
});
// Mở lại
$('.btn-molai').off('click').on('click', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var con = confirm("Do you want to accept this account?");
    if (con) {
        $.ajax({
            url: "/AdminManager/ManagerUserAdmin/ChangeStatusReopen/" + id,
            type: "POST",
            dataType: "json",
            success: function (res) {
                if (res.status == true) {
                    swal("Thông báo", "Successful approval", "success")
                        .then((value) => {
                            window.location.href = "/AdminManager/ManagerUserAdmin/Index";
                        });
                } else {
                    swal({
                        title: "Thông báo",
                        text: "Failure to approve",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true
                    })
                        .then((value) => {
                            window.location.href = "/AdminManager/ManagerUserAdmin/Index";
                        });
                }
            }
        });
    }
});