namespace Model.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RequestEmergency")]
    public partial class RequestEmergency
    {
        public int ID { get; set; }

        public long UserID { get; set; }

        [Required]
        [StringLength(250)]
        public string Location { get; set; }

        [StringLength(250)]
        public string Name { get; set; }

        [StringLength(250)]
        public string Address { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        public int? Phone { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? UpdateDate { get; set; }

        public int? BrowserBy { get; set; }

        public int? Status { get; set; }
        public Double lat { get; set; }
        public Double lng { get; set; }
        public int DisplayMap { get; set; }
    }
}
