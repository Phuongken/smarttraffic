﻿
using System.Web.Mvc;

namespace SmartTraffic.Areas.AdminManager
{
    public class AdminManagerAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "AdminManager";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
               name: "Notification",
               url: "Notification",
               defaults: new { controller = "NotificationAdmin", action = "Index", id = UrlParameter.Optional },
               namespaces: new[] { "SmartTraffic.Areas.AdminManager.Controllers" }
           );

            context.MapRoute(
               name: "Feedback of user admin",
               url: "Feedback",
               defaults: new { controller = "FeedbackAdmin", action = "Index", id = UrlParameter.Optional },
               namespaces: new[] { "SmartTraffic.Areas.AdminManager.Controllers" }
           );

            context.MapRoute(
               name: "Role admin",
               url: "ManagerRole",
               defaults: new { controller = "RoleAdmin", action = "Index", id = UrlParameter.Optional },
               namespaces: new[] { "SmartTraffic.Areas.AdminManager.Controllers" }
           );

            context.MapRoute(
               name: "Manager user admin",
               url: "ManagerUser",
               defaults: new { controller = "ManagerUserAdmin", action = "Index", id = UrlParameter.Optional },
               namespaces: new[] { "SmartTraffic.Areas.AdminManager.Controllers" }
           );

            context.MapRoute(
               name: "Complain admin",
               url: "Complain",
               defaults: new { controller = "ComplainAdmin", action = "Index", id = UrlParameter.Optional },
               namespaces: new[] { "SmartTraffic.Areas.AdminManager.Controllers" }
           );

            context.MapRoute(
               name: "Login admin",
               url: "admin/login",
               defaults: new { controller = "LoginAdmin", action = "Index", id = UrlParameter.Optional },
               namespaces: new[] { "SmartTraffic.Areas.AdminManager.Controllers" }
           );

            context.MapRoute(
                "AdminManager_default",
                "AdminManager/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                 namespaces: new[] { "SmartTraffic.Areas.AdminManager.Controllers" }
            );
        }
    }
}