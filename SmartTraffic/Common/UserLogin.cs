﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartTraffic.Common
{
    [Serializable]
    public class UserLogin
    {
        public long UserID { get; set; }

        public string LastName { get; set; }

        public string FirstName { get; set; }

        public string Email { get; set; }

        public int? Phone { get; set; }

        public string AddressDetail { get; set; }

    }
}