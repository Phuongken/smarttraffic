﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model.DAO;
using Model.EF;

namespace SmartTraffic.Areas.AdminManager.Controllers
{
    public class RoleAdminController : Controller
    {
        RoleDao dao = new RoleDao();
        // GET: AdminManager/RoleAdmin
        public ActionResult Index()
        {
            var role = dao.ListRole();
            return View(role);
        }

        // GET: AdminManager/RoleAdmin/Create
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Role newRole)
        {
            if (ModelState.IsValid)
            {
                bool role = dao.Insert(newRole);
                if (role == true)
                {
                    return RedirectToAction("Index", "RoleAdmin");
                }
            }
            return View(newRole);
        }
        public ActionResult Edit(int id)
        {
            var result = dao.Find(id);
            return View(result);
        }

        // POST: AdminManager/NewAdmin/Edit/5
        [HttpPost]
        public ActionResult Edit(Role role, int id)
        {
            if (ModelState.IsValid)
            {
                var result = dao.Update(role);
                if (result == true)
                {
                    return RedirectToAction("Index", "RoleAdmin");
                }
                else
                {
                    ModelState.AddModelError("", "Chỉnh sửa role thất bại");
                }
            }
            return View(role);
        }

        // POST: AdminManager/NewAdmin/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id)
        {
            var result = dao.Delete(id);
            if (result == true)
            {
                return Json(new
                {
                    status = true
                });
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }
        }
    }
}
