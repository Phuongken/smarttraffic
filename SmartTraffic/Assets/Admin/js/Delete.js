﻿var cart = {
    init: function () {
        cart.regEvents();
    },
    regEvents: function () {
        // delete role
        $('.delete_Role').off('click').on('click', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var con = confirm("Bạn có muốn xóa role này không");
            if (con) {
                $.ajax({
                    url: "/AdminManager/RoleAdmin/Delete/" + id,
                    type: "POST",
                    dataType: "json",
                    success: function (res) {
                        if (res.status == true) {
                            swal("Thông báo", "Xóa role thành công.", "success")
                                .then((value) => {
                                    window.location.href = "/AdminManager/RoleAdmin/Index";
                                });
                        } else {
                            swal({
                                title: "Thông báo",
                                text: "Chưa xóa được role.",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true
                            })
                                .then((value) => {
                                    window.location.href = "/AdminManager/RoleAdmin/Index";
                                });
                        }
                    }
                });
            }
        });
        // delete notification
        $('.delete_Notificateion').off('click').on('click', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var con = confirm("Do you want to delete this notification.");
            if (con) {
                $.ajax({
                    url: "/AdminManager/NotificationAdmin/Delete/" + id,
                    type: "POST",
                    dataType: "json",
                    success: function (res) {
                        if (res.status == true) {
                            swal("Thông báo", "Locked Notificate success.", "success")
                                .then((value) => {
                                    window.location.href = "/Notification";
                                });
                        } else {
                            swal({
                                title: "Thông báo",
                                text: "Unable to delete notificateion.",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true
                            })
                                .then((value) => {
                                    window.location.href = "/Notification";
                                });
                        }
                    }
                });
            }
        });
    }
}
cart.init();
