﻿using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.DAO
{
    public class NewDao
    {
        DataModel db = null;

        public NewDao()
        {
            db = new DataModel();
        }
        
        public List<New> ListNew()
        {
            List<New> list = db.News.ToList();
            return list;
        }
    }
}
