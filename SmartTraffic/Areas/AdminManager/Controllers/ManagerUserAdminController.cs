﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Model.DAO;
using Model.EF;
using SmartTraffic.Common;
using SmartTraffic.Models;

namespace SmartTraffic.Areas.AdminManager.Controllers
{
    public class ManagerUserAdminController : Controller
    {
        DataModel db = new DataModel();
        ManagerUserDao dao = new ManagerUserDao();
        // GET: AdminManager/ManagerUserAdmin
        //Random rand = new Random();

        //public const string Alphabet =
        //"abcdefghijklmnopqrstuvwyxzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        //public string GenerateString(int size)
        //{
        //    char[] chars = new char[size];
        //    for (int i = 0; i < size; i++)
        //    {
        //        chars[i] = Alphabet[rand.Next(Alphabet.Length)];
        //    }
        //    return new string(chars);
        //}
        public ActionResult Index()
        {
            //Random r = new Random();
            //int rInt = r.Next( 11); //for ints
            //ViewBag.test = rInt;
            var result = (from u in db.Users orderby (u.Status == 2) descending select u);
            return View(result);
        }
        public ActionResult mailApproved()
        {
            return View();
        }
        public ActionResult mailRefuse()
        {
            return View();
        }
        public ActionResult mailLock()
        {
            return View();
        }
        public ActionResult mailReopen()
        {
            return View();
        }
        //Chấp nhận
        public JsonResult ChangeStatusApproved(int id)
        {
            //
            SendMail sendMail = new SendMail();
            var result1 = dao.getUserByMail(id);
            var txtNew = dao.Find(id);
            var admin = (AdminLogin)Session["AdminSession"];
            var adminID = admin.UserID;
            //var getOrderDetail = (from c in db.Contracts join o in db.OrderDetails on c.ID equals o.ContractID where c.ID == id select o).FirstOrDefault();
            var result = dao.ChangeStatusApproved(id, Convert.ToInt32(adminID));
            if (result == true)
            {

                string content = System.IO.File.ReadAllText(Server.MapPath("/Areas/AdminManager/Views/ManagerUserAdmin/mailApproved.cshtml"));
                content = content.Replace("{{ID}}", txtNew.ID.ToString());
                content = content.Replace("{{FirstName}}", txtNew.FirstName);
                content = content.Replace("{{LastName}}", txtNew.LastName);
                content = content.Replace("{{Email}}", txtNew.Email);

                sendMail.SendEmail(result1, "SmartTrafic team", content);

                Session["SendMailSuccsess"] = 1;
                return Json(new
                {
                    status = true
                });
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }

        }
        // Từ chối
        public JsonResult ChangeStatusRefuse(int id)
        {
            SendMail sendMail = new SendMail();
            var result1 = dao.getUserByMail(id);
            var txtNew = dao.Find(id);
            var admin = (AdminLogin)Session["AdminSession"];
            var adminID = admin.UserID;
            //var getOrderDetail = (from c in db.Contracts join o in db.OrderDetails on c.ID equals o.ContractID where c.ID == id select o).FirstOrDefault();
            var result = dao.ChangeStatusRefuse(id, Convert.ToInt32(adminID));
            if (result == true)
            {

                string content = System.IO.File.ReadAllText(Server.MapPath("/Areas/AdminManager/Views/ManagerUserAdmin/mailRefuse.cshtml"));
                content = content.Replace("{{ID}}", txtNew.ID.ToString());
                content = content.Replace("{{FirstName}}", txtNew.FirstName);
                content = content.Replace("{{LastName}}", txtNew.LastName);
                content = content.Replace("{{Email}}", txtNew.Email);

                sendMail.SendEmail(result1, "SmartTrafic team", content);

                Session["SendMailSuccsess"] = 1;
                return Json(new
                {
                    status = true
                });
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }

        }
        // Khóa
        public JsonResult ChangeStatusLock(int id)
        {
            SendMail sendMail = new SendMail();
            var result1 = dao.getUserByMail(id);
            var txtNew = dao.Find(id);
            var admin = (AdminLogin)Session["AdminSession"];
            var adminID = admin.UserID;
            //var getOrderDetail = (from c in db.Contracts join o in db.OrderDetails on c.ID equals o.ContractID where c.ID == id select o).FirstOrDefault();
            var result = dao.ChangeStatusLock(id, Convert.ToInt32(adminID));
            if (result == true)
            {

                string content = System.IO.File.ReadAllText(Server.MapPath("/Areas/AdminManager/Views/ManagerUserAdmin/mailLock.cshtml"));
                content = content.Replace("{{ID}}", txtNew.ID.ToString());
                content = content.Replace("{{FirstName}}", txtNew.FirstName);
                content = content.Replace("{{LastName}}", txtNew.LastName);
                content = content.Replace("{{Email}}", txtNew.Email);

                sendMail.SendEmail(result1, "SmartTrafic team", content);

                Session["SendMailSuccsess"] = 1;
                return Json(new
                {
                    status = true
                });
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }

        }
        // Mở lại
        public JsonResult ChangeStatusReopen(int id)
        {
            SendMail sendMail = new SendMail();
            var result1 = dao.getUserByMail(id);
            var txtNew = dao.Find(id);
            var admin = (AdminLogin)Session["AdminSession"];
            var adminID = admin.UserID;
            //var getOrderDetail = (from c in db.Contracts join o in db.OrderDetails on c.ID equals o.ContractID where c.ID == id select o).FirstOrDefault();
            var result = dao.ChangeStatusReopen(id, Convert.ToInt32(adminID));
            if (result == true)
            {

                string content = System.IO.File.ReadAllText(Server.MapPath("/Areas/AdminManager/Views/ManagerUserAdmin/mailReopen.cshtml"));
                content = content.Replace("{{ID}}", txtNew.ID.ToString());
                content = content.Replace("{{FirstName}}", txtNew.FirstName);
                content = content.Replace("{{LastName}}", txtNew.LastName);
                content = content.Replace("{{Email}}", txtNew.Email);

                sendMail.SendEmail(result1, "SmartTrafic team", content);

                Session["SendMailSuccsess"] = 1;
                return Json(new
                {
                    status = true
                });
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }

        }
    }
}